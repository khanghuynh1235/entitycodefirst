﻿namespace Banking_Website.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? ContactInfo { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
