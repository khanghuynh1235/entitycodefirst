﻿namespace Banking_Website.Models
{
    public class Log
    {
        public int LogId { get; set; }
        public DateTime LogDateTime { get; set; }
        public string? LogType { get; set; }
        public string? Content { get; set; }

        public int? TransactionId { get; set; }
        public int? ReportId { get; set; }

        public Transaction Transaction { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
    }
}
