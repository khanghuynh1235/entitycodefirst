﻿namespace Banking_Website.Models
{
    public class Account
    {
        public int AccountId { get; set; }
        public string? AccountNumber { get; set; }
        public decimal Balance { get; set; }
        public string? AccountType { get; set; }

        public int CustomerId { get; set; }

        public Customer Customer { get; set; }
        public virtual ICollection<Report> Reports { get; set; }

    }
}
