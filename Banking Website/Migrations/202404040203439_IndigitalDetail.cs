﻿namespace Banking_Website.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IndigitalDetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        AccountNumber = c.String(),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountType = c.String(),
                        CustomerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        ContactInfo = c.String(),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        TransactionId = c.Int(nullable: false, identity: true),
                        TransactionDate = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransactionType = c.String(),
                        AccountId = c.Int(nullable: false),
                        Employee_EmployeeId = c.Int(),
                        Customer_CustomerId = c.Int(),
                    })
                .PrimaryKey(t => t.TransactionId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.Employee_EmployeeId)
                .ForeignKey("dbo.Customers", t => t.Customer_CustomerId)
                .Index(t => t.AccountId)
                .Index(t => t.Employee_EmployeeId)
                .Index(t => t.Customer_CustomerId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        ContactInfo = c.String(),
                    })
                .PrimaryKey(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Logs",
                c => new
                    {
                        LogId = c.Int(nullable: false, identity: true),
                        LogDateTime = c.DateTime(nullable: false),
                        LogType = c.String(),
                        Content = c.String(),
                        TransactionId = c.Int(),
                        ReportId = c.Int(),
                    })
                .PrimaryKey(t => t.LogId)
                .ForeignKey("dbo.Transactions", t => t.TransactionId)
                .Index(t => t.TransactionId);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        ReportId = c.Int(nullable: false, identity: true),
                        ReportName = c.String(),
                        ReportDate = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                        account_AccountId = c.Int(),
                        log_LogId = c.Int(),
                        transaction_TransactionId = c.Int(),
                    })
                .PrimaryKey(t => t.ReportId)
                .ForeignKey("dbo.Accounts", t => t.account_AccountId)
                .ForeignKey("dbo.Logs", t => t.log_LogId)
                .ForeignKey("dbo.Transactions", t => t.transaction_TransactionId)
                .Index(t => t.account_AccountId)
                .Index(t => t.log_LogId)
                .Index(t => t.transaction_TransactionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Transactions", "Customer_CustomerId", "dbo.Customers");
            DropForeignKey("dbo.Logs", "TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.Reports", "transaction_TransactionId", "dbo.Transactions");
            DropForeignKey("dbo.Reports", "log_LogId", "dbo.Logs");
            DropForeignKey("dbo.Reports", "account_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Transactions", "Employee_EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.Transactions", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Reports", new[] { "transaction_TransactionId" });
            DropIndex("dbo.Reports", new[] { "log_LogId" });
            DropIndex("dbo.Reports", new[] { "account_AccountId" });
            DropIndex("dbo.Logs", new[] { "TransactionId" });
            DropIndex("dbo.Transactions", new[] { "Customer_CustomerId" });
            DropIndex("dbo.Transactions", new[] { "Employee_EmployeeId" });
            DropIndex("dbo.Transactions", new[] { "AccountId" });
            DropIndex("dbo.Accounts", new[] { "CustomerId" });
            DropTable("dbo.Reports");
            DropTable("dbo.Logs");
            DropTable("dbo.Employees");
            DropTable("dbo.Transactions");
            DropTable("dbo.Customers");
            DropTable("dbo.Accounts");
        }
    }
}
